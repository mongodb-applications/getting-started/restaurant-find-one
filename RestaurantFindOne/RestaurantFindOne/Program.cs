﻿using MongoDB.Driver;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Conventions;
using MongoDB.Driver.Linq;

namespace RestaurantFindOne;

public class FindOne
{
    private static IMongoCollection<Restaurant>? restaurantsCollection;
    private const string MongoConnectionString = "mongodb+srv://spravce:JYHoYzXaRFMvZah5@learn-mongodb-cluster.lkbhbsw.mongodb.net/?retryWrites=true&w=majority";

    public static void Main(string[] args)
    {
        
        if (string.IsNullOrEmpty(MongoConnectionString))
        {
            Environment.Exit(0);
        }
        
        Setup();

        // Find one document using builders
        Console.WriteLine("Finding a document with builders...");
        FindOneRestaurantBuilder();

        // Extra space for console readability 
        Console.WriteLine();

        // Find one document using LINQ
        Console.WriteLine("Finding a document with LINQ...");
        FindOneRestaurantLinq();
    }

    private static void FindOneRestaurantBuilder()
    {
        // start-find-builders
        // change from var filter to filterdefinition<Restaurant>
        FilterDefinition<Restaurant> filter = Builders<Restaurant>.Filter
            .Eq(r => r.Name, "Bagels N Buns");

        Restaurant restaurant = restaurantsCollection.Find(filter).FirstOrDefault();
        // end-find-builders

        Console.WriteLine(restaurant.ToBsonDocument());
        Console.WriteLine(restaurant.Name);

    }

    private static void FindOneRestaurantLinq()
    {
        // start-find-linq
        var restaurant = restaurantsCollection.AsQueryable()
            .Where(r => r.Name == "Bagels N Buns").FirstOrDefault();
        // end-find-linq

        Console.WriteLine(restaurant.ToBsonDocument());
    }

    private static void Setup()
    {
        // This allows automapping of the camelCase database fields to our models. 
        // changed from var to conventionpack
        ConventionPack camelCaseConvention = new ConventionPack { new CamelCaseElementNameConvention() };
        ConventionRegistry.Register("CamelCase", camelCaseConvention, type => true);

        // Establish the connection to MongoDB and get the restaurants database
        IMongoClient mongoClient = new MongoClient(MongoConnectionString);
        IMongoDatabase restaurantsDatabase = mongoClient.GetDatabase("sample_restaurants");
        restaurantsCollection = restaurantsDatabase.GetCollection<Restaurant>("restaurants");
    }
}
